# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/c1user/Programming/Ygor/src/main.cpp" "/home/c1user/Programming/Ygor/CMakeFiles/Ygor.dir/src/main.cpp.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "NUSE_OPAQUE_IDENTIFIERS=1"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "."
  "src"
  "src/parseInput"
  "src/AdventureMode"
  "meta"
  "meta/include"
  "meta/deps/cpptoml/include"
  "meta/build"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "/home/c1user/Programming/Ygor/src/parseInput/CMakeFiles/parseInput.dir/DependInfo.cmake"
  "/home/c1user/Programming/Ygor/src/AdventureMode/CMakeFiles/adventureParser.dir/DependInfo.cmake"
  "/home/c1user/Programming/Ygor/src/AdventureMode/CMakeFiles/adventureItemReader.dir/DependInfo.cmake"
  "/home/c1user/Programming/Ygor/src/AdventureMode/CMakeFiles/adventureSemAnalysis.dir/DependInfo.cmake"
  "/home/c1user/Programming/Ygor/src/parseInput/CMakeFiles/depItem.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
