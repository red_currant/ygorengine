/**
 * @file adventureItemReader.cpp
 * @author Anon
 */

#include <vector>
#include <iostream>
#include <sstream>
#include <fstream>
#include "adventureItemReader.h"
#include "depItem.h"

using namespace std;

item roomItem(string room)
{
  string file = "build/data/" + room;
  string output;
  string itemList;
  stringstream ItemStream(itemList);
  string segment;
  int id;
  int lastCmd;
  string name;
  string cmdText = "";
  item item;


  ifstream infile (file);
  while (getline(infile,output))
  {
    // hold onto the block of text until it's needed
    if (output.find(":") == string::npos)
    {
      if (cmdText != "")
        cmdText.append("\n");
      cmdText.append(output);
      continue;
    }
    else if (item.commands.size() > 0)
    {
      // apply the block fo text to the previous command
      itemCmd *command = &item.commands[lastCmd];
      if (command->command != "" && cmdText != "")
        command->command.append("\n");
      command->command.append(cmdText);
      cmdText = "";
      command = NULL;
    }
    // set the command value
    string value = "";
    if (output.find(": ") != string::npos)
      value = output.substr(output.find(": ")+2);
    // find the command name
    if (output.substr(0, output.find(":")) == "name")
    {
      name = value;
    }
    else if (output.substr(0, output.find(":")) == "id")
    {
      id = stoi(value);
    }
    else if (output.substr(0, 2) == "v-")
    {
      //if it's a variable
    }
    else
    {
      itemCmd cmd;
      //seperate the words that can call the command
      string names = output.substr(0, output.find(":"));
      istringstream iss(names);
      string name;
      while (getline(iss, name, '/'))
      {
        cmd.names.push_back(name);
      }
      cmd.command = value;
      lastCmd = item.commands.size();
      item.commands.push_back(cmd);
    }
  }
  item.id = id;
  item.names.push_back(name);
  return item;
}

void itemsInRoom(string room, shared_ptr<vector<item>> items)
{
  string file = "build/data/" + room;
  string output;
  string itemList;
  ifstream infile (file);
  while (getline(infile,output))
  {
    if (output.substr(0, output.find(":")) == "item")
    {
      itemList = output.substr(output.find(": ")+2);
    }
  }
  infile.close();
  stringstream ItemStream(itemList);
  string segment;
  int id;
  int lastCmd;
  string nameList;
  string adjtvList;
  string cmdText = "";
  item tempItem;
  //vector<item> items;

  while(std::getline(ItemStream, segment, ' '))
  {
    tempItem.filename = segment + ".itm";
    file = "build/data/items/" + tempItem.filename;
    ifstream infile (file);
    while (getline(infile,output))
    {
      // hold onto the block of text until it's needed
      if (output.find(":") == string::npos)
      {
        if (cmdText != "")
          cmdText.append("\n");
        cmdText.append(output);
        continue;
      }
      else if (tempItem.commands.size() > 0)
      {
        // apply the block fo text to the previous command
        itemCmd *command = &tempItem.commands[lastCmd];
        if (command->command != "" && cmdText != "")
          command->command.append("\n");
        command->command.append(cmdText);
        cmdText = "";
        command = NULL;
      }
      // set the command value
      string value = "";
      if (output.find(": ") != string::npos)
        value = output.substr(output.find(": ")+2);
      // find the command name
      if (output.substr(0, output.find(":")) == "id")
      {
        id = stoi(value);
      }
      else if (output.substr(0, output.find(":")) == "name")
      {
        nameList = value;
      }
      else if (output.substr(0, output.find(":")) == "adjectives")
      {
        adjtvList = value;
      }
      else if (output.substr(0, output.find(":")) == "prep")
      {
        //if it's a preposition
      }
      else if (output.substr(0, 2) == "v-")
      {
        var variable;
        variable.name = output.substr(2, output.find(":"));
        variable.value = value;
        tempItem.vars.push_back(variable);
      }
      else
      {
        itemCmd cmd;
        //seperate the words that can call the command
        string names = output.substr(0, output.find(":"));
        istringstream iss(names);
        string name;
        while (getline(iss, name, '/'))
        {
          cmd.names.push_back(name);
        }
        cmd.command = value;
        lastCmd = tempItem.commands.size();
        tempItem.commands.push_back(cmd);
        // TODO parse command here, and remove the string command
        // part of the itemCmd struct
      }
    }
    // apply the text to the last object
    itemCmd *command = &tempItem.commands[lastCmd];
    if (command->command != "" && cmdText != "")
      command->command.append("\n");
    command->command.append(cmdText);
    cmdText = "";
    command = NULL;
    // TODO parse command here, and remove the string command
    // part of the itemCmd struct
    tempItem.id = id;
    istringstream iss(nameList);
    while (iss >> nameList)
      tempItem.names.push_back(nameList);
    istringstream issa(adjtvList);
    while (issa >> adjtvList)
      tempItem.adjtvs.push_back(adjtvList);
    items->push_back(tempItem);
  }
  return;
}

shared_ptr<vector<item>> advItemRead(string room)
{
  shared_ptr<vector<item>> items (new vector<item>());
  itemsInRoom(room, items);
  item roomItm = roomItem(room);
  roomItm.names.push_back("room");
  items->push_back(roomItm);

  return items;
}
