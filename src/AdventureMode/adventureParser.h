#ifndef ADVENTUREPARSER_H
#define ADVENTUREPARSER_H

#include <vector>
#include <string>
#include "depItem.h"

using namespace std;

typedef enum {
  identsym, numbersym, listsym, externalsym,
  appendsym, prependsym,
  escquotesym,
  timessym, slashsym, plussym, minussym,
  assign, expressym, termsym, factorsym,
  elementsym, prepsym,
  eql, neq, lss, leq, gtr, geq,
  eqls, pluseqls, timeseqls, minuseqls, slasheqls,
  andsym, orsym,
  ifsym, elsesym, whilesym, contain,
  varsym, programsym, textsym, conditionsym,
  printsym, newlinesym, charsym, assignmentsym,
  null} Symbol;

struct Node {
  Symbol symbol;
  std::string data;
  std::vector<Node> ch;
};

struct command {
  std::string verb;
  std::string noun;
  std::vector<std::string> adjtvs;
};

struct itemCmd {
  std::vector<std::string> names;
  Node cmnd;
  std::string command;
};

struct var {
  std::string name;
  std::string value;
};

struct item {
  std::string filename;
  int id;
  std::vector<std::string> names;
  std::vector<std::string> adjtvs;
  std::vector<var> vars;
  std::vector<itemCmd> commands;
};

class Parser {
private:
  Node iterateThroughItems(shared_ptr<vector<item>> items, command cmd);
  Node parseCommand(itemCmd cmd, item itm);

  bool ident(string::iterator& cmd, Node& nd);
  bool external(string::iterator& cmd, Node& nd);
  bool preposition(string::iterator& cmd, Node& nd);
  bool element(string::iterator& cmd, Node& nd);
  bool single(string::iterator& cmd, Node& nd);
  bool list(string::iterator& cmd, Node& nd);
  bool factor(string::iterator& cmd, Node& nd);
  bool term(string::iterator& cmd, Node& nd);
  bool expression(string::iterator& cmd, Node& nd);
  bool condition(string::iterator& cmd, Node& nd);
  bool assignment(string::iterator& cmd, Node& nd);
  bool container(string::iterator& cmd, Node& nd);
  bool whilestatement(string::iterator& cmd, Node& nd);
  bool ifstatement(string::iterator& cmd, Node& nd);
  bool append(string::iterator& cmd, Node& nd);
  bool prepend(string::iterator& cmd, Node& nd);
  bool print(string::iterator& cmd, Node& nd);
  bool text(string::iterator& cmd, Node& nd);
  bool statement(string::iterator& cmd, Node& nd);
  bool variable(string::iterator& cmd, Node& nd);
  bool block(string::iterator& cmd, Node& nd);

  bool leftparen(string::iterator& cmd);
  bool rightparen(string::iterator& cmd);
  bool leftcurly(string::iterator& cmd);
  bool rightcurly(string::iterator& cmd);
  bool appendfunc(string::iterator& cmd);
  bool prependfunc(string::iterator& cmd);
  bool times(string::iterator& cmd, Node& nd);
  bool slash(string::iterator& cmd, Node& nd);
  bool plusfunc(string::iterator& cmd, Node& nd);
  bool minusfunc(string::iterator& cmd, Node& nd);
  bool equal(string::iterator& cmd, Node& nd);
  bool notequal(string::iterator& cmd, Node& nd);
  bool lessthan(string::iterator& cmd, Node& nd);
  bool lesseql(string::iterator& cmd, Node& nd);
  bool greaterthan(string::iterator& cmd, Node& nd);
  bool greatereql(string::iterator& cmd, Node& nd);
  bool equals(string::iterator& cmd, Node& nd);
  bool plusequals(string::iterator& cmd, Node& nd);
  bool minusequals(string::iterator& cmd, Node& nd);
  bool timesequals(string::iterator& cmd, Node& nd);
  bool slashequals(string::iterator& cmd, Node& nd);
  bool andfunc(string::iterator& cmd, Node& nd);
  bool orfunc(string::iterator& cmd, Node& nd);
  bool lineend(string::iterator& cmd);
  bool whilefunc(string::iterator& cmd);
  bool iffunc(string::iterator& cmd);
  bool elsefunc(string::iterator& cmd);
  bool quote(string::iterator& cmd);
  bool escquote(string::iterator& cmd, string& str);
  bool newline(string::iterator& cmd, string& str);
  bool character(string::iterator& cmd, string& str);
  bool numberfunc(string::iterator& cmd, Node& nd);
  bool endline(string::iterator& cmd);

public:
  shared_ptr<vector<Node>> advParse(const std::shared_ptr<std::vector<depItm>> depList, const std::shared_ptr<std::vector<item>> items);
};

#endif
