#ifndef ADVENTUREITEMREADER_H
#define ADVENTUREITEMREADER_H

#include <vector>
#include <string>
#include "depItem.h"
#include "adventureParser.h"

std::shared_ptr<std::vector<item>> advItemRead(std::string room);

#endif
