#ifndef ADVENTURESEMANALYSIS_H
#define ADVENTURESEMANALYSIS_H

#include "adventureParser.h"

void analyze(std::shared_ptr<std::vector<Node>> cmds);

#endif
