/**
 * @file adventureParser.cpp
 * @author Anon
 */

#include <iostream>
#include <sstream>
#include <fstream>
#include <ctype.h>
#include "adventureParser.h"

using namespace std;

void skipWhitespace(string::iterator& cmd)
{
  while (*cmd == ' ' || *cmd == '\t')
    cmd = cmd + 1;
}

bool match(string::iterator& cmd, string token)
{
  for (auto tok : token)
  {
    if (*cmd != tok)
      return false;
    cmd = cmd + 1;
  }
  return true;
}

/*******************************************
 * Containers
 ******************************************/
bool Parser::leftparen(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "("))
    return true;

  cmd = origional;
  return false;
}

bool Parser::rightparen(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, ")"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::leftcurly(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "{"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::rightcurly(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "}"))
    return true;

  cmd = origional;
  return false;
}

/*******************************************
 * list operators
 ******************************************/
bool Parser::appendfunc(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "<+"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::prependfunc(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, ">+"))
    return true;

  cmd = origional;
  return false;
}

/*******************************************
 * Math
 ******************************************/
bool Parser::times(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node times;
  times.symbol = timessym;
  if (match(cmd, "*")) {
    nd.ch.push_back(times);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::slash(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node slash;
  slash.symbol = slashsym;
  if (match(cmd, "/")) {
    nd.ch.push_back(slash);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::plusfunc(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node plusstate;
  plusstate.symbol = plussym;
  if (match(cmd, "+")) {
    nd.ch.push_back(plusstate);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::minusfunc(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node minusstate;
  minusstate.symbol = minussym;
  if (match(cmd, "-")) {
    nd.ch.push_back(minusstate);
    return true;
  }

  cmd = origional;
  return false;
}

/*******************************************
 * Checking for equality, greater than, etc.
 ******************************************/
bool Parser::equal(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node equal;
  equal.symbol = eql;
  if (match(cmd, "==")) {
    nd.ch.push_back(equal);
    return true;
  }
  cmd = origional;
  return false;
}

bool Parser::notequal(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node notequal;
  notequal.symbol = neq;
  if (match(cmd, "!=")) {
    nd.ch.push_back(notequal);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::lessthan(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node lessthan;
  lessthan.symbol = lss;
  if (match(cmd, "<")) {
    nd.ch.push_back(lessthan);
    return true;
  }
  cmd = origional;
  return false;
}

bool Parser::lesseql(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node lesseql;
  lesseql.symbol = leq;
  if (match(cmd, "<=")) {
    nd.ch.push_back(lesseql);
    return true;
  }
  cmd = origional;
  return false;
}

bool Parser::greaterthan(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node greaterthan;
  greaterthan.symbol = gtr;
  if (match(cmd, ">")) {
    nd.ch.push_back(greaterthan);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::greatereql(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node greatereql;
  greatereql.symbol = geq;
  if (match(cmd, ">=")) {
    nd.ch.push_back(greatereql);
    return true;
  }

  cmd = origional;
  return false;
}

/*******************************************
 * assignment operators
 ******************************************/
bool Parser::equals(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node equals;
  equals.symbol = eqls;
  if (match(cmd, "=")) {
    nd.ch.push_back(equals);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::plusequals(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node plusequals;
  plusequals.symbol = pluseqls;
  if (match(cmd, "+=")) {
    nd.ch.push_back(plusequals);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::minusequals(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node minusequals;
  minusequals.symbol = minuseqls;
  if (match(cmd, "-=")) {
    nd.ch.push_back(minusequals);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::timesequals(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node timesequals;
  timesequals.symbol = timeseqls;
  if (match(cmd, "*=")) {
    nd.ch.push_back(timesequals);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::slashequals(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node slashequals;
  slashequals.symbol = slasheqls;
  if (match(cmd, "/=")) {
    nd.ch.push_back(slashequals);
    return true;
  }

  cmd = origional;
  return false;
}

/*******************************************
 * And / Or
 ******************************************/
bool Parser::andfunc(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node ands;
  ands.symbol = andsym;
  if (match(cmd, "&&")) {
    nd.ch.push_back(ands);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::orfunc(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node ors;
  ors.symbol = orsym;
  if (match(cmd, "||")) {
    nd.ch.push_back(ors);
    return true;
  }

  cmd = origional;
  return false;
}

/*******************************************
 * Misc tokens and characters
 ******************************************/
bool Parser::lineend(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "\n"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::whilefunc(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "while"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::iffunc(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "if"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::elsefunc(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "else"))
    return true;

  cmd = origional;
  return false;
}

bool Parser::quote(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "\""))
    return true;

  cmd = origional;
  return false;
}

bool Parser::escquote(string::iterator& cmd, string& str)
{
  string::iterator origional = cmd;

  if (match(cmd, "\\\"")) {
    str.append("\"");
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::newline(string::iterator& cmd, string& str)
{
  string::iterator origional = cmd;

  if (match(cmd, "\\n")) {
    str.append("\n");
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::character(string::iterator& cmd, string& str)
{
  string::iterator origional = cmd;

  if (*cmd != '"')
  {
    str.push_back(*cmd);
    cmd++;
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::numberfunc(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node num;
  num.symbol = numbersym;
  if (!isdigit(*cmd)){
    cmd = origional;
    return false;
  }

  do{
    if (isalpha(*cmd)) {
      cmd = origional;
      return false;
    } else if (!isdigit(*cmd)) {
      nd.ch.push_back(num);
      return true;
    }
    num.data.push_back(*cmd);
    cmd++;
  }while(*cmd != ' ' && *cmd != '\t' && *cmd != '\n');

  nd.ch.push_back(num);
  return true;
}

bool Parser::endline(string::iterator& cmd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (match(cmd, "\n"))
    return true;

  cmd = origional;
  return false;
}

/*******************************************
 * big boy tokens
 ******************************************/
bool Parser::ident(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node ident;
  ident.symbol = identsym;
  if (!isalpha(*cmd))
    return false;

  ident.data = "";
  do{
    ident.data.push_back(*cmd);
    cmd++;
  }while(isalnum(*cmd) || *cmd == '_');

  nd.ch.push_back(ident);
  return true;
}

bool Parser::external(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node external;
  external.symbol = externalsym;
  if (!ident(cmd, external)) {
    cmd = origional;
    return false;
  }
  if (!match(cmd, ".")) {
    cmd = origional;
    return false;
  }
  if (!ident(cmd, external)) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(external);
  return true;
}

bool Parser::preposition(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node prep;
  prep.symbol = prepsym;
  if (!match(cmd, "prep-")) {
    cmd = origional;
    return false;
  }

  if (!ident(cmd, prep)) {
    cmd = origional;
    return false;
  }
  nd.ch.push_back(prep);
  return true;
}

bool Parser::element(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);
  Node element;
  element.symbol = elementsym;
  if (!(external(cmd, element) || preposition(cmd, element) ||
        ident(cmd, element))) {
    cmd = origional;
    return false;
  }

  skipWhitespace(cmd);
  if (!match(cmd, "<")) {
    cmd = origional;
    return false;
  }
  if (!numberfunc(cmd, element)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);
  if (!match(cmd, ">")) {
    cmd = origional;
    return false;
  }
  nd.ch.push_back(element);
  return true;
}

bool Parser::single(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (!(element(cmd, nd) || external(cmd, nd) ||
        preposition(cmd, nd) || ident(cmd, nd))) {
    cmd = origional;
    return false;
  }

  return true;
}

bool Parser::list(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);
  if (!single(cmd, nd)) {
    cmd = origional;
    return false;
  }

  skipWhitespace(cmd);
  while (match(cmd, ",") && list(cmd, nd)) {
    skipWhitespace(cmd);
  }
  return true;
}

bool Parser::factor(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node factor;
  factor.symbol = factorsym;
  if (single(cmd, factor)) {
    nd.ch.push_back(factor);
    return true;
  } else if (numberfunc(cmd, factor)) {
    nd.ch.push_back(factor);
    return true;
  } else if (text(cmd, factor)) {
    nd.ch.push_back(factor);
    return true;
  } else if (leftparen(cmd)) {
    if (!expression(cmd, factor)) {
      cmd = origional;
      return false;
    }
    if (!rightparen(cmd)){
      cmd = origional;
      return false;
    }
    nd.ch.push_back(factor);
    return true;
  }

  cmd = origional;
  return false;
}

bool Parser::term(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node term;
  term.symbol = termsym;
  if (!factor(cmd, term)){
    cmd = origional;
    return false;
  }

  while ((times(cmd, term) || slash(cmd, term)) &&
         factor(cmd, term)) {
    ;
  }

  nd.ch.push_back(term);
  return true;
}

bool Parser::expression(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node exp;
  exp.symbol = expressym;
  if (!term(cmd, exp)){
    cmd = origional;
    return false;
  }

  while ((plusfunc(cmd, exp) || minusfunc(cmd, exp)) &&
         term(cmd, exp)) {
    ;
  }

  nd.ch.push_back(exp);
  return true;
}

bool Parser::condition(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node cd;
  cd.symbol = conditionsym;
  if (!expression(cmd, cd)) {
    cmd = origional;
    return false;
  }

  if (!(equal(cmd, cd) || notequal(cmd, cd) ||
        lessthan(cmd, cd) || lesseql(cmd, cd) ||
        greaterthan(cmd, cd) || greatereql(cmd, cd))) {
    cmd = origional;
    return false;
  }

  if (!expression(cmd, cd)) {
    cmd = origional;
    return false;
  }

  while ((andfunc(cmd, cd) || orfunc(cmd, cd)) && condition(cmd, cd)) {
    ;
  }

  nd.ch.push_back(cd);
  return true;
}

bool Parser::assignment(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node as;
  as.symbol = assign;
  if (!list(cmd, as)) {
    cmd = origional;
    return false;
  }

  if (!(equals(cmd, as) || plusequals(cmd, as) ||
        minusequals(cmd, as) || timesequals(cmd, as) ||
        slashequals(cmd, as))) {
    cmd = origional;
    return false;
  }

  if (!(list(cmd, as) || expression(cmd, as))) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(as);
  return true;
}

bool Parser::container(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node container;
  container.symbol = contain;
  if (!leftcurly(cmd)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);
  lineend(cmd);

  while (statement(cmd, container)) {
    ;
  }
  skipWhitespace(cmd);

  if (!rightcurly(cmd)) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(container);
  return true;
}

bool Parser::whilestatement(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node whilestate;
  whilestate.symbol = whilesym;
  if (!whilefunc(cmd)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);

  if (!leftparen(cmd)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);

  if (!condition(cmd, whilestate)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);

  if (!rightparen(cmd)) {
    cmd = origional;
    return false;
  }

  skipWhitespace(cmd);
  lineend(cmd);

  if (!container(cmd, whilestate)) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(whilestate);
  return true;
}

bool Parser::ifstatement(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node ifstate;
  ifstate.symbol = ifsym;
  if (!iffunc(cmd)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);

  if (!leftparen(cmd)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);

  if (!condition(cmd, ifstate)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);

  if (!rightparen(cmd)) {
    cmd = origional;
    return false;
  }
  skipWhitespace(cmd);
  lineend(cmd);

  if (!container(cmd, ifstate)) {
     cmd = origional;
    return false;
  }

  Node elsestate;
  elsestate.symbol = elsesym;
  while(elsefunc(cmd)) {
    if (!ifstatement(cmd, elsestate) && !container(cmd, elsestate)) {
      cmd = origional;
      return false;
    }
    if (elsestate.ch.size() > 0) {
      ifstate.ch.push_back(elsestate);
    }
  }

  nd.ch.push_back(ifstate);
  return true;
}

bool Parser::append(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node append;
  append.symbol = appendsym;
  if (!single(cmd, append)) {
    cmd = origional;
    return false;
  }

  if (!appendfunc(cmd)) {
    cmd = origional;
    return false;
  }

  if (!list(cmd, append)) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(append);
  return true;
}

bool Parser::prepend(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node prepend;
  prepend.symbol = prependsym;
  if (!single(cmd, prepend)) {
    cmd = origional;
    return false;
  }

  if (!prependfunc(cmd)) {
    cmd = origional;
    return false;
  }

  if (!list(cmd, prepend)) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(prepend);
  return true;
}

bool Parser::print(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (!match(cmd, "<<")) {
    cmd = origional;
    return false;
  }
  Node print;
  print.symbol = printsym;
  if (!list(cmd, print)) {
    cmd = origional;
    return false;
  }
  nd.ch.push_back(print);
  return true;
}

bool Parser::text(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (!quote(cmd)) {
    cmd = origional;
    return false;
  }
  string str = "";
  while (newline(cmd, str) || escquote(cmd, str) ||
         character(cmd, str)) {
    ;
  }

  if (!quote(cmd)) {
    cmd = origional;
    return false;
  }
  Node text;
  text.symbol = textsym;
  text.data = str;
  nd.ch.push_back(text);
  return true;
}

bool Parser::statement(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  if (!(text(cmd, nd) || ifstatement(cmd, nd) ||
        whilestatement(cmd, nd) || assignment(cmd, nd) ||
        print(cmd, nd) || append(cmd, nd) ||
        prepend(cmd, nd))) {
    cmd = origional;
    return false;
  }

  if (!lineend(cmd)) {
    cmd = origional;
    return false;
  }

  return true;
}

bool Parser::variable(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  Node variable;
  variable.symbol = varsym;
  if (!match(cmd, "VAR ")) {
    cmd = origional;
    return false;
  }

  if (!list(cmd, variable)) {
    cmd = origional;
    return false;
  }

  nd.ch.push_back(variable);
  return true;
}

bool Parser::block(string::iterator& cmd, Node& nd)
{
  string::iterator origional = cmd;
  skipWhitespace(cmd);

  variable(cmd, nd);
  lineend(cmd);
  skipWhitespace(cmd);
  while (statement(cmd, nd)) {
    ;
  }
  return true;
}

void printTree(Node& nd, int tab)
{
  for (int i = 0; i < tab; i++)
    cout << " ";
  switch(nd.symbol) {
  case programsym:
    cout << "program";
    break;
  case varsym:
    cout << "variable";
    break;
  case textsym:
    cout << "text: ";
    cout << nd.data;
    break;
  case printsym:
    cout << "print";
    break;
  case appendsym:
    cout << "append";
    break;
  case prependsym:
    cout << "prepend";
    break;
  case ifsym:
    cout << "if";
    break;
  case elsesym:
    cout << "else";
    break;
  case whilesym:
    cout << "while";
    break;
  case assign:
    cout << "assignment";
    break;
  case identsym:
    cout << "ident: ";
    cout << nd.data;
    break;
  case numbersym:
    cout << "number: ";
    cout << nd.data;
    break;
  case elementsym:
    cout << "element ";
    cout << nd.data;
    break;
  case prepsym:
    cout << "preposition";
    break;
  case externalsym:
    cout << "external";
    break;
  case contain:
    cout << "container";
    break;
  case conditionsym:
    cout << "condition";
    break;
  case expressym:
    cout << "expression";
    break;
  case termsym:
    cout << "term";
    break;
  case factorsym:
    cout << "factor";
    break;
  case andsym:
    cout << "and";
    break;
  case orsym:
    cout << "or";
    break;
  case timessym:
    cout << "*";
    break;
  case slashsym:
    cout << "/";
    break;
  case plussym:
    cout << "+";
    break;
  case minussym:
    cout << "-";
    break;
  case eqls:
    cout << "=";
    break;
  case pluseqls:
    cout << "+=";
    break;
  case timeseqls:
    cout << "*=";
    break;
  case minuseqls:
    cout << "-=";
    break;
  case slasheqls:
    cout << "/=";
    break;
  case eql:
    cout << "==";
    break;
  case neq:
    cout << "!=";
    break;
  case lss:
    cout << "<";
    break;
  case leq:
    cout << "<=";
    break;
  case gtr:
    cout << ">";
    break;
  case geq:
    cout << ">=";
    break;
  default:
    cout << "unknown";
    break;
  }
  cout << endl;
  tab++;
  for (Node node : nd.ch) {
    printTree(node, tab);
  }
}

// uses a recursive decent parser to parse the command;
Node Parser::parseCommand(itemCmd cmd, item itm)
{
  //cout << cmd.command << endl;
  Node program;
  program.symbol = programsym;
  string::iterator begin = cmd.command.begin();
  block(begin, program);
  printTree(program, 0);
  return program;
}

unique_ptr<vector<command>> coreCommands(shared_ptr<vector<depItm>> depList)
{
  vector<command> commands;
  command cmd;
  int currentCmd = 0;
  int i = 0;
  if (depList->size() == 1)
  {
    cmd.verb = depList->at(0).node2->word;
    cmd.noun = "room";
    commands.push_back(cmd);
  }
  for (depItm item : *depList)
  {
    switch(item.type){
    case depType::dobj:
      cmd.verb = item.node1->word;
      cmd.noun = item.node2->word;
      currentCmd = i;
      i++;
      commands.push_back(cmd);
      break;
    case depType::nn:
      cmd.verb = item.node1->word;
      cmd.noun = item.node2->word;
      currentCmd = i;
      i++;
      commands.push_back(cmd);
    case depType::amod:
      commands[currentCmd].adjtvs.push_back(item.node2->word);
      break;
    }
    /*cout << depTypeToString(item.type);
      cout << "(";
      cout << item.node1->word << ", " << item.node2->word;
      cout << ")" << endl;*/
  }

  return make_unique<vector<command>>(commands);
}

// go through every item, every name of every item, every item command, and every name for every item command
Node Parser::iterateThroughItems(shared_ptr<vector<item>> items, command cmd)
{
  Node nullNode;
  nullNode.symbol = null;
  nullNode.data = "";
  for (item itm : *items) {
    for (int i = 0; i < itm.names.size(); i++) {
      if (itm.names[i] == cmd.noun) {
        for (itemCmd itmCmd : itm.commands) {
          for (string name : itmCmd.names) {
            if (name == cmd.verb) {
              return parseCommand(itmCmd, itm);
              // after the item is found move to the next command
            }
          }
        }
        return nullNode;
      }
    }
  }
  return nullNode;
}

shared_ptr<vector<Node>> Parser::advParse(const shared_ptr<vector<depItm>> depList, const shared_ptr<vector<item>> items)
{
  vector<command> commands = *coreCommands(depList);
  vector<Node> parsedCmds;
  for (command cmd : commands)
  {
    parsedCmds.push_back(iterateThroughItems(items, cmd));
  }

  return make_shared<vector<Node>>(parsedCmds);
}
