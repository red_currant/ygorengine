/**
 * @file main.cpp
 * @author Anon
 */

#include <algorithm>
#include <vector>
#include <iostream>
#include <fstream>
#include <chrono>
#include "adventureItemReader.h"
#include "adventureParser.h"
#include "adventureSemAnalysis.h"
#include "parseInput.h"

//#include "meta/parser/sr_parser.h"
#include "meta/sequence/sequence.h"
//#include "meta/sequence/perceptron.h"
#include "meta/analyzers/analyzer.h"
#include "meta/caching/all.h"
#include "meta/corpus/document.h"
#include "meta/index/inverted_index.h"
#include "meta/index/ranker/ranker_factory.h"
#include "meta/parser/analyzers/tree_analyzer.h"
#include "meta/sequence/analyzers/ngram_pos_analyzer.h"
#include "meta/util/time.h"

using namespace std;

void blockPrint(int id)
{
  switch(id){
  case 0:
    cout << "This is a test build of the Ygor engine\n"
         << "Version 0.1\n";
    break;
  case 1:
    cout << "Created by Anon\n";
    cout << "Powerd by the Ygor Engine\n"; //Ygor stands for "You GOtta' Read"
    cout << "NLP by the MeTA Toolkit\n";
    cout << "MeTA Toolkit ACL Demo Paper\n"
         << "\tSean Massung, Chase Geigle, and ChengXiang Zhai\n"
         << "\tMeTA: A Unified Toolkit for Text Retrieval and Analysis\n"
         << "\tProceedings of ACL-2016 System Demonstrations\n"
         << "\tAugust 2016\n"
         << "\tBerlin, Germany\n"
         << "\tAssociation for Computational Linguistics\n"
         << "\tPages 91--96\n"
         << "\thttp://anthology.aclweb.org/P16-4016\n";
    break;
  case 2:
    cout << "The Ygor Engine is subject to copyright\n";
    cout << "The adventure text is subject to copyright\n";
    cout << "MeTA is licensed under the MIT License\n";
    break;
  }
}

void get(string file, string verb)
{
  string output;
  ifstream infile (file);
  while(getline(infile, output))
  {
    if (verb == output.substr(0, output.find(":")))
    {
      cout << output.substr(output.find(": ")+2) << endl;
    }
  }
  infile.close();
  return;
}

int main()
{
  blockPrint(0);
  // Register additional analyzers
  meta::sequence::register_analyzers();
  meta::parser::register_analyzers();

  // Setup parsers
  cout << "Loading tagging model" << endl;
  meta::sequence::perceptron tagger{"models/perceptron-tagger"};
  meta::sequence::perceptron* taggerPtr = &tagger;
  cout << "Loading parser model" << endl;
  meta::parser::sr_parser parser{"models/parser"};
  meta::parser::sr_parser* parserPtr = &parser;
  cout << "Finished loading\n";
  get("build/data/index.ygor", "look");
  string cmd;
  string room = "index.ygor";
  string * it = new string("");
  while (1 == 1)
  {
    cmd.clear();
    cout << ">";
    getline(cin, cmd);
    auto start = chrono::steady_clock::now();
    transform(cmd.begin(), cmd.end(), cmd.begin(), ::tolower);
    if (cmd == "exit" || cmd == "quit")
    {
      cout << "Exiting, Please Wait...\n";
      break;
    }
    else if (cmd == "credits" || cmd == "credit")
    {
      blockPrint(1);
    }
    else if (cmd == "legal")
    {
      blockPrint(2);
    }
    else
    {
      //TODO move this so that it's only called when moving to a new room
      shared_ptr<vector<item>> items = advItemRead(room);
      //TODO "it" should be a pointer
      shared_ptr<vector<depItm>> depList = parseInput(cmd, taggerPtr, parserPtr, it);
      Parser parser;
      shared_ptr<vector<Node>> cmds = parser.advParse(depList, items);
      analyze(cmds);
    }
    auto end = chrono::steady_clock::now();
    auto diff = end - start;
    cout << "Elapsed Time:"
         << chrono::duration <double, milli> (diff).count()
         << " ms"
         << endl;
  }
  return 0;
}
