#ifndef DEPNODE_H
#define DEPNODE_H

#include <string>
#include <vector>
#include <memory>
#include "meta/meta.h"

//The node that makes up a dependency tree

struct depNode
{
  meta::class_label label; // this label is the part of speech or phrase part
  std::string word; // this is the actual word
  std::vector<std::shared_ptr<depNode>> children;// if the vector is of size 0 it has no children
};

#endif
