/**
 * @file phrase_finder.h
 * @author Anon
 */

#ifndef META_PARSER_PHRASE_FINDER_H_
#define META_PARSER_PHRASE_FINDER_H_

#include <memory>
#include <vector>
#include <string>
#include "depNode.h"

#include "meta/meta.h"
#include "meta/parser/trees/visitors/visitor.h"

namespace meta
{
namespace parser
{

class phrase_finder : public const_visitor<void>
{
  public:
    void operator()(const leaf_node&) override;
    void operator()(const internal_node&) override;
    void start();

    depNode * dependencyNode();

  private:
    std::shared_ptr<depNode> head;
    std::shared_ptr<depNode> localHead;
    void childNodeToParent(std::shared_ptr<depNode> node);
    int rank_label(class_label * label);
    void printDepTree(depNode * node);
    int nodeNum;
    std::vector<std::unique_ptr<internal_node>> nodes_;
};
}
}

#endif
