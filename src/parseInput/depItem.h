#ifndef DEPITEM_H
#define DEPITEM_H

#include "depNode.h"

// All of the dependencies
// Yes, all of them
// They are all here if I need them, though I doubt all will be used
enum depType
{
  acomp,
  advcl,
  advmod,
  agent,
  amod,
  appos,
  aux,
  auxpass,
  cc,
  ccomp,
  conj,
  cop,
  csubj,
  csubjpass,
  dep,
  det,
  discourse,
  dobj,
  expltv,
  goeswith,
  iobj,
  mark,
  mwe,
  neg,
  nn,
  npadvmod,
  nsubj,
  nsubjpass,
  num,
  number,
  parataxis,
  pcomp,
  pobj,
  poss,
  possessive,
  preconj,
  predet,
  prep,
  prepc,
  prt,
  punct,
  quantmod,
  remod,
  ref,
  root,
  tmod,
  vmod,
  xcomp,
  xsubj
};

// a pair of dependency nodes paired by a type
struct depItm
{
  depType type;
  depNode * node1;//First node in the dependency
  depNode * node2;//The node that depends on the above
};

// convert a dependency type to a string
std::string depTypeToString(depType type);

#endif
