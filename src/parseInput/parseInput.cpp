/**
 * @file parseInput.cpp
 * @author Anon
 */

#include <vector>
#include <algorithm>
#include <iostream>
#include "phrase_finder.cpp"
#include "parseInput.h"
#include "depNode.h"
#include "depItem.h"

// parsers and analysers
//#include "meta/parser/sr_parser.h"
#include "meta/sequence/sequence.h"
//#include "meta/sequence/perceptron.h"
#include "meta/analyzers/analyzer.h"
#include "meta/analyzers/tokenizers/icu_tokenizer.h"
#include "meta/analyzers/filters/all.h"
#include "meta/analyzers/ngram/ngram_word_analyzer.h"
#include "meta/caching/all.h"
#include "meta/corpus/document.h"
#include "meta/index/inverted_index.h"
#include "meta/index/ranker/ranker_factory.h"
#include "meta/parser/analyzers/tree_analyzer.h"
#include "meta/sequence/analyzers/ngram_pos_analyzer.h"
#include "meta/util/time.h"

//visitors
#include "meta/parser/trees/visitors/leaf_node_finder.h"
#include "meta/parser/trees/internal_node.h"
#include "meta/parser/trees/leaf_node.h"

using namespace std;
using namespace meta;

// print the entire tree
void printDepTree(depNode * node)
{
  std::cout << "\n(";
  std::cout << node->word << "|" << node->children.size();
  for (int i = 0; i < node->children.size(); i++)
  {
    printDepTree(&*node->children[i]);
  }
  std::cout << ")";
  return;
}

// get the type of dependency based on word labels
depType findDepType(class_label node1, class_label node2)
{
  depType dType;
  if (node1 == class_label("ROOT"))
  {
    dType = root;
  }
  if (node1 == class_label("VB"))
  {
    if (node2 == class_label("NN")){
      dType = dobj;
    } else if (node2 == class_label("CC")){
      dType = cc;
    } else if (node2 == class_label("VB")){
      dType = conj;
    } else if (node2 == class_label("VB")){
      dType = advcl;
    } else if (node2 == class_label("RB")){
      dType = advmod;
    } else if (node2 == class_label("RP")){
      dType = prt;
    } else if (node2 == class_label("MD")){
      dType = aux;
    } else if (node2 == class_label("TO") ||
               node2 == class_label("IN")){
      dType = mark;
    } else if (node2 == class_label("PRP")){
      dType = dobj;
    } else if (node2 == class_label("VBP")){
      dType = advcl;
    }
  } else if (node1 == class_label("VBD"))
    {
      if (node2 == class_label("NN")){
        dType = dobj;
      } if (node2 == class_label("VB")){
        dType = advcl;
      } else if (node2 == class_label("VBG")){
        dType = advcl;
      } else if (node2 == class_label("WP")){
        dType = nsubj;
      } else if (node2 == class_label("PRP")){
        dType = nsubj;
      }
    } else if (node1 == class_label("VBP")){
    if (node2 == class_label("IN")){
      dType = mark;
    } else if (node2 == class_label("NN")){
      dType = dobj;
    } else if (node2 == class_label("PRP")){
      dType = nsubj;
    } else if (node2 == class_label("VBD")){
      dType = ccomp;
    }
  } else if (node1 == class_label("VBG"))
    {
      if (node2 == class_label("NN")){
        dType = nsubj;
      } else if (node2 == class_label("IN")){
        dType = mark;
      } else if (node2 == class_label("VBD")){
        dType = aux;
      }
  } else if (node1 == class_label("VBZ"))
    {
      if (node2 == class_label("PRP")){
        dType = nsubj;
      } else if (node2 == class_label("JJ")){
        dType = acomp;
      } else if (node2 == class_label("NN")){
        dType = dobj;
      }
  } else if (node1 == class_label("NN"))
  {
    if (node2 == class_label("DT")){
      dType = det;
    } else if (node2 == class_label("NN")){
      dType = nn;
    } else if (node2 == class_label("CC")){
      dType = cc;
    } else if (node2 == class_label("CD")){
      dType = num;
    } else if (node2 == class_label("JJ")){
      dType = amod;
    } else if (node2 == class_label("TO")){
      //dType = prep;
    } else if (node2 == class_label("IN")){
      dType = prep;
    } else if (node2 == class_label("POS")){
      dType = possessive;
    } else if (node2 == class_label("PRP$")){
      dType = poss;
    }
  } else if (node1 == class_label("NNS")){
    if (node2 == class_label("NN")){
      dType = nn;
    }
  } else if (node1 == class_label("JJ"))
  {
    if (node2 == class_label("RB")){
      dType = advmod;
    } else if (node2 == class_label("NN")){
      dType = nsubj;
    } else if (node2 == class_label("DT")){
      dType = det;
    } else if (node2 == class_label("JJ")){
      dType = conj;
    } else if (node2 == class_label("CC")){
      dType = cc;
    } else if (node2 == class_label("VBZ")){
      dType = cop;
    }
  } else if (node1 == class_label("PRP"))
  {
    if (node2 == class_label("TO")){
      //dType = prep;
    }
  } else if (node1 == class_label("CD")){
    if (node2 == class_label("CD")){
      dType = number;
    }
  }
  return dType;
}

// get the dependencies of the tree
void dependencyList(depNode * node, shared_ptr<vector<depItm>> depList)
{
  for (int i = 0; i < node->children.size(); i++)
  {
    depItm item;
    depNode * nextNode = &*node->children[i];
    if (!(nextNode->word == node->word &&
          nextNode->label == node->label))
    {
      // create the dependency item
      item.node1 = node;
      item.node2 = nextNode;
      item.type =
        findDepType(
                    item.node1->label,
                    item.node2->label);
      // add the item to the list
      depList->push_back(item);
    }
    // if the next node has children, then call the function on it
    if(nextNode->children.size() > 0)
      dependencyList(nextNode, depList);
  }
  return;
}

// take in a stream and return a properly formatted sequence of "tokens" or words
sequence::sequence tokenSequence(string cmd)
{
  // construct the token filter chain
  std::unique_ptr<analyzers::token_stream> stream
    = make_unique<analyzers::tokenizers::icu_tokenizer>();
  //stream = make_unique<analyzers::filters::lowercase_filter>(std::move(stream));
  // stemming
  /*stream = make_unique<analyzers::filters::porter2_filter>(std::move(stream));
  stream = make_unique<analyzers::filters::empty_sentence_filter>(std::move(stream));*/

  stream = make_unique<analyzers::filters::ptb_normalizer>(std::move(stream));
  stream->set_content(std::move(cmd));

  sequence::sequence seq;

  while (*stream)
  {
    auto token = stream->next();
    if (token == "<s>")
    {
      seq = {};
    }
    else if (token == "</s>")
    {
      break;
    }
    else
    {
      seq.add_symbol(sequence::symbol_t{token});
    }
  }
  return seq;
}

// Clarify what the word "it" refers to
string clarifyIt(sequence::perceptron * tagger)
{
  string sentence;
  string it;
  while(it.empty())
  {
    cout << "What do you mean by \"it\"?\n?>";
    getline(cin, sentence);
    sequence::sequence seq = tokenSequence(sentence);
    tagger->tag(seq);
    for (int i = 0; i < seq.size(); i++)
    {
      if ((string)seq[i].tag() == "NN")
      {
        it = (string)seq[i].symbol();
      } else if ((string)seq[i].tag() == "PRP" &&
                 it.empty())
      {
        it = (string)seq[i].symbol();
      }
    }
    if (it.empty())
    {
      cout << "Couln't find a noun in that. ";
      it = "";
    }
    else if (it == "it")
    {
      cout << "Haha, very funny. ";
      it = "";
    }
  }
  return it;
}

// the main function that parses the input
shared_ptr<vector<depItm>> parseInput(string cmd,sequence::perceptron * tagger, parser::sr_parser * parser, string * it)
{
  // TODO spell checking

  // construct the token filter chain
  sequence::sequence seq = tokenSequence(cmd);

  tagger->tag(seq);
  auto tree = parser->parse(seq);
  //tree.pretty_print(std::cout);

  // create dependency based parse output
  parser::phrase_finder pf;
  pf.start();
  tree.visit(pf);
  depNode * depHead = pf.dependencyNode();
  depHead->word = "ROOT";
  depHead->label = class_label("ROOT");
  //printDepTree(depHead);
  shared_ptr<vector<depItm>> depList (new vector<depItm>());
  dependencyList(depHead, depList);

  // find the noun and set that as "it"
  // if there's an "it" replace it with the latest noun
  for (depItm item : *depList)
  {
    if (item.node1->label == class_label("NN"))
    {
      it->assign(item.node1->word);
    } else if (item.node1->word.compare("it") == 0)
    {
      if (it->empty())
      {
        it->assign(clarifyIt(tagger));
      }
      item.node1->word = *it;
    }
    if (item.node2->label == class_label("NN"))
    {
      it->assign(item.node2->word);
    } else if (item.node2->word.compare("it") == 0)
    {
      if (it->empty())
      {
        it->assign(clarifyIt(tagger));
      }
      item.node2->word = *it;
    }
  }

  return depList;
}
