#include <string>
#include "depItem.h"

std::string depTypeToString(depType type)
{
  switch(type){
  case acomp:      return "acomp";
  case advcl:      return "advcl";
  case advmod:     return "advmod";
  case agent:      return "agent";
  case amod:       return "amod";
  case appos:      return "appos";
  case aux:        return "aux";
  case auxpass:    return "auxpass";
  case cc:         return "cc";
  case ccomp:      return "ccomp";
  case conj:       return "conj";
  case cop:        return "cop";
  case csubj:      return "csubj";
  case csubjpass:  return "csubjpass";
  case dep:        return "dep";
  case det:        return "det";
  case discourse:  return "discourse";
  case dobj:       return "dobj";
  case expltv:     return "expl";
  case goeswith:   return "goeswith";
  case iobj:       return "iobj";
  case mark:       return "mark";
  case mwe:        return "mwe";
  case neg:        return "neg";
  case nn:         return "nn";
  case npadvmod:   return "npadvmod";
  case nsubj:      return "nsubj";
  case nsubjpass:  return "nsubjpass";
  case num:        return "num";
  case number:     return "number";
  case parataxis:  return "parataxis";
  case pcomp:      return "pcomp";
  case pobj:       return "pobj";
  case poss:       return "poss";
  case possessive: return "possessive";
  case preconj:    return "preconj";
  case predet:     return "predet";
  case prep:       return "prep";
  case prepc:      return "prepc";
  case prt:        return "prt";
  case punct:      return "punct";
  case quantmod:   return "quantmod";
  case remod:      return "remod";
  case ref:        return "ref";
  case root:       return "root";
  case tmod:       return "tmod";
  case vmod:       return "vmod";
  case xcomp:      return "xcomp";
  case xsubj:      return "xsubj";
  }
  return "xx";
}
