//header guard
#ifndef PARSEINPUT_H
#define PARSEINPUT_H

#include <vector>
#include <string>
#include "depItem.h"
#include "meta/parser/sr_parser.h"
#include "meta/sequence/perceptron.h"

//things that are accessable from other places
std::shared_ptr<std::vector<depItm>> parseInput(std::string cmd, meta::sequence::perceptron * tagger, meta::parser::sr_parser * parser, std::string * it);

#endif
