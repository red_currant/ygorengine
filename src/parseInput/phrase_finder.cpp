/**
 * @file phrase_finder.cpp
 * @author Anon
 */

#include <iostream>
#include <string>
#include "depNode.h"

#include "phrase_finder.h"
#include "meta/parser/trees/node.h"
#include "meta/parser/trees/leaf_node.h"
#include "meta/parser/trees/internal_node.h"
#include "meta/util/shim.h"

namespace meta
{
namespace parser
{
// preforms the operator on an internal node
void phrase_finder::operator()(const leaf_node& node)
{
  // create a new leaf node
  std::shared_ptr<depNode> leaf(new depNode());
  // set the node to have the same label and word as the incoming node
  leaf->label = node.category();
  leaf->word = *node.word();
  // make the node a child of the local head node
  localHead->children.push_back(move(leaf));
  childNodeToParent(localHead);
  return;
}

// preforms the operator on an internal node
void phrase_finder::operator()(const internal_node& node)
{
  /* You may be wondering why there are three heads.
   * Well, each head serves a purpose.
   * localHead is used to hold the value from the leaf nodes
   * prevHead is used to hold the node imediantly above the current node
   * holdHead holds onto a particular instance of an internal node
   */
  // create a new node
  std::shared_ptr<depNode> holdHead(new depNode());
  // set the previous node to what was the head
  std::shared_ptr<depNode> prevHead = localHead;
  // set localHead to be the current head
  localHead = holdHead;
  // Set the default label and word
  localHead->label = class_label("XX");
  localHead->word = std::to_string(nodeNum++);
  // Recursively call the current function
  node.each_child([&](const parser::node* child)
                  {
                    child->accept(*this);
                    // update the local head so that it stays local
                    localHead = holdHead;
                  });
  if (prevHead != NULL)// you can't go to a node that doesn't exist
  {
    // add the current node to be a child of the previous node
    prevHead->children.emplace_back(move(localHead));
    // nothing leaves the SBAR
    if (node.category() != class_label("SBAR"))
    {
      // set the children
      childNodeToParent(prevHead);
    }
  }
  /*else
  {
    // print the tree
    printDepTree(&*localHead);
  }*/
}

// called before everything else to prevent nulls
void phrase_finder::start()
{
  nodeNum = 0;
}

// assign a parent node to one of the children nodes
void phrase_finder::childNodeToParent(std::shared_ptr<depNode> node)
{
  // If the node has children
  if(node->children.size() > 0)
  {
    // if the node is using the defaults, set it to be equal to the first of it's children.
    if (node->label == class_label("XX"))
    {
      node->label = node->children[0]->label;
      node->word = node->children[0]->word;
    }
    // loop through all the children
    for (int i = 0; i < node->children.size(); i++)
    {
      // if the current label in parent has less priority than one of the children, make the parent node higher.
      if (rank_label(&node->label) < rank_label(&node->children[i]->label))
      {
        node->label = node->children[i]->label;
        node->word = node->children[i]->word;
      }
    }
  }
  return;
}

// takes in a label and ranks it's place
int phrase_finder::rank_label(class_label * label)
{
  // convert the label to a string then get the first letter
  std::string strLabel = (std::string)*label;
  switch(strLabel[0])
  {
  case 'V':
    if (*label == class_label("VBG") ||
        *label == class_label("VB"))
      return 5;
    return 4;
    break;
  case 'N':
    if (*label == class_label("NNS"))
      return 3;
    return 2;
    break;
  case 'J':
    return 1;
    break;
  case 'P':
    if (*label == class_label("PRP$"))
      return 1;
    return 2;
    break;
  case 'R':
    return 0;
    break;
  case 'C':
    return 0;
    break;
  case 'D':
    return 0;
  case 'M':
    return 0;
  case 'W':
    return 0;
  case 'T':
    return 0;
  default:
    return 0;
    break;
  }
}

// Print the dependency tree
void phrase_finder::printDepTree(depNode * node)
{
  std::cout << "\n(";
  std::cout << node->word << "|" << node->children.size();
  for (int i = 0; i < node->children.size(); i++)
  {
    printDepTree(&*node->children[i]);
  }
  std::cout << ")";
  return;
}

// returns the dependency node
depNode * phrase_finder::dependencyNode()
{
  return &*localHead;
}
}
}
